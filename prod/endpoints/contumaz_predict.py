from fastapi import APIRouter, Body, Form, HTTPException
from src.config.config import get_settings, Prod_Settings, Dev_Settings
from src import predict_contumaz
from datetime import date, datetime
from typing import Optional
import os
from src.utils.api_utils import ResponseModel


router = APIRouter()

today = f"{datetime.now():%Y-%m-%d  %H:%M:%S}"


# carregar arquivo contendo as classes para treinamento
@router.post("/v1/contumaz_predict", name="Predizer modelo", description="Classificar os contumazes prioritários")

async def contumaz_predict(empresa_id: int = Form(...), 
                     date: str = today, 
                     environment: str = Form(...),
                     filter_appointment_date: Optional[date] = Body(None)):
    
    '''Predizer algoritmo
    empresa_id: id da empresa no banco de dados - campo idEmpresa
    environment: env ou prod
    filter_appointment_date: data de competencia
    '''
    
    # load config
    settings = get_settings(environment)
    
    # apply process
    filter_date = f"{filter_appointment_date}"
    data = predict_contumaz.run(empresa_id, filter_date, settings)
    
    return ResponseModel(data, "Success")

