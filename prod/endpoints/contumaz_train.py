from fastapi import APIRouter, Form, HTTPException
from src import train_contumaz
from datetime import datetime
import os
from src.utils.api_utils import ResponseModel
from src.config.config import get_settings, Prod_Settings, Dev_Settings

router = APIRouter()

today = f"{datetime.now():%Y-%m-%d  %H:%M:%S}"

# realizar treino e carregar arquivo joblib no gcp
@router.post("/v1/contumaz_train", name="Treinar modelo", description="Treinar algoritmo para classificar os contumazes prioritários")

async def contumaz_train(environment: str = Form(...), 
                   date: str = today):
    
    '''Treinar algoritmo
    environment: env ou prod
    '''
    settings = get_settings(environment)
    train_contumaz.run(settings)
    
    data = {"env":environment, 
            "date":date,
            "message":"Dados treinados com sucesso"}
    
    return ResponseModel(data, "Success")
    
