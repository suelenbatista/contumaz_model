from fastapi import APIRouter

router = APIRouter()

#home page
@router.get("/", name = "Home Page", description = "API details.")
async def home_page():
    """API Documentation Page."""
    return {"message": "This endpoint was created by Dasa DS group! Access the /docs endpoint to check all other options"}


