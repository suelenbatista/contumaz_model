# general
import numpy as np
import pandas as pd



# ml models
from sklearn.model_selection import train_test_split
from sklearn.metrics import  confusion_matrix
from sklearn.model_selection import cross_validate
from sklearn.model_selection import GridSearchCV
import xgboost as xgb
from joblib import dump

# visualization
import matplotlib.pyplot as plt 
import seaborn as sns

# api
import logging


log = logging.getLogger("uvicorn")





# train xgBoost Classifier
def training_process(df, settings):
    
    '''Training the model
    df: final pandas dataframe preprocessed'''
    
    # train/test split
    X = df.drop('alto_custo', axis=1)
    y = df['alto_custo'] 
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify = y, random_state=42, test_size=0.25)
    
    
    # Classifier and Gridsearch
    xgb_model = xgb.XGBClassifier(random_state=42,verbosity=0,use_label_encoder=False)
    
    clf = GridSearchCV(xgb_model,
                       {"eta": [0.05, 0.10, 0.20] ,
                        'max_depth': [2, 3, 4],
                        'n_estimators': [100,150,200,250],
                        'max_delta_step':[1,10],
                        'scale_pos_weight':[2,5,10]},
                       n_jobs=-1, 
                   cv=5,scoring='f1')

    clf.fit(X_train,y_train)
    
    ## Save evaluation metrics
    y_pred = clf.predict(X_test)
    
    # save cross validation
    log.info(f"Cross validation initiated")
    
    evaluation_metrics = ['balanced_accuracy', 'accuracy', 'precision_macro', 'recall_macro', 'f1_macro']
    cross_validation_results = cross_validate(clf, X,y, cv=5, scoring=evaluation_metrics)
    cross_validation_results = pd.DataFrame.from_dict(cross_validation_results)
    cv_path = f'{settings.ROOT_DIR}/src/ml_models/cv_xgBoostmodel_results.csv'
    cross_validation_results.to_csv(cv_path, index = False, sep = ';', decimal = ',')
    
    log.info(f"Cross validation finished")
    
    # save confusion matrix
    cm_path = f'{settings.ROOT_DIR}/src/ml_models/cm_xgBoost_results.png'
    cm = confusion_matrix(y_test,y_pred)
    plt.figure(figsize=(15,15))
    sns.heatmap(cm, annot=True,cmap='Blues', fmt='g')
    plt.title('Confusion Matrix')
    plt.savefig(cm_path)
    
    # final training
    log.info(f"Training final model")
    clf.fit(X, y)
    # save model pipeline
    xgBoost_model_path = f'{settings.ROOT_DIR}/src/ml_models/xgBoost_model.joblib'
    dump(clf, xgBoost_model_path)
    
    print(f'Model trained')
