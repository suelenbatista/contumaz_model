WITH contumazes AS
  (SELECT DISTINCT idPessoa,
                   ContumazConsultaEletiva AS contumaz_eletiva,
                   ContumazConsultaPS AS contumaz_ps,
                   ContumazExames AS contumaz_exames
   FROM `{project}.{dataset}.view_fato_classificacao_vidas`
   WHERE dtcompetencia ='2018-12-01' ),---data atual: quem vai ser alto custo daqui 3 meses?
     cronicos AS
  (SELECT idPessoa,
          MAX(Cronico_Flag) AS cronico
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_classificacao_vidas`
   WHERE dtcompetencia BETWEEN DATE('2018-01-01') AND DATE('2018-12-01')
   GROUP BY idPessoa), ---Quantidade de exames
exames AS
  (SELECT idPessoa,
          count(idPassagem) AS exames_qtd
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_sinistro`
   WHERE AMB_dsClassProcedimento='Exames'
     AND dtAtendimentoContaPaga BETWEEN DATE('2018-01-01') AND DATE('2018-12-01')
   GROUP BY idPessoa), ---Quantidades de atendimentos de pronto socorro
 pronto_socorro AS
  (SELECT idPessoa,
          count(idPassagem) AS ps_qtd
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_sinistro`
   WHERE AMB_dsClassProcedimento='Pronto-Socorro'
     AND dtAtendimentoContaPaga BETWEEN DATE('2018-01-01') AND DATE('2018-12-01')
   GROUP BY idPessoa), --Atendimento de consulta eletiva
 consulta_eletiva AS
  (SELECT idPessoa,
          count(idPassagem) AS eletiva_qtd
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_sinistro`
   WHERE AMB_dsClassProcedimento='Consulta Eletiva'
     AND dtAtendimentoContaPaga BETWEEN DATE('2018-01-01') AND DATE('2018-12-01')
   GROUP BY idPessoa), --sinistro
sinistro_12_meses AS---melhor fazer tipo histórico de sinistro
  (SELECT idPessoa,
          sum(cast(vrPagoProcedimentoContaPaga AS FLOAT64)) AS sinistro
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_sinistro`
   WHERE dtAtendimentoContaPaga BETWEEN DATE('2018-01-01') AND DATE('2018-12-01')
   GROUP BY idPessoa), --Flag de internação
flag_internacao AS
  (SELECT DISTINCT idPessoa,
                   cdTipoContaPaga_Tratado AS internacao
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_sinistro`
   WHERE dtAtendimentoContaPaga BETWEEN DATE('2018-01-01') AND DATE('2018-12-01')--data
AND cdTipoContaPaga_Tratado=2 ), --Alto custo como target
target AS
  (SELECT idPessoa,
          AltoCusto_Flag AS alto_custo
   FROM `laboratorio-eng-dados.fenix_bi.view_fato_classificacao_vidas`
   WHERE dtcompetencia= '2019-03-01'
   GROUP BY idPessoa,AltoCusto_Flag),
-- Seleção do idEmpresa 
idempresa AS 
(SELECT DISTINCT 
                        idPessoa
                        ,idEmpresa 
    FROM `{project}.{dataset}.view_fato_classificacao_vidas`
    WHERE idEmpresa= NUMBER_ID_EMPRESA)
SELECT a.idPessoa,
       contumaz_eletiva,
       contumaz_ps,
       contumaz_exames,
       cronico,
       IFNULL(exames_qtd,0) as exames_qtd,
       IFNULL(ps_qtd,0) as ps_qtd,
       IFNULL(eletiva_qtd,0) as eletiva_qtd,
       CASE
           WHEN internacao = 2 THEN 1
           ELSE 0
       END AS internacao,
        IFNULL(sinistro,0) as sinistro,
       alto_custo
FROM contumazes a
inner JOIN target g ON a.idPessoa=g.idPessoa
inner JOIN idempresa i on a.idPessoa=i.idPessoa
LEFT JOIN exames b ON a.idPessoa =b.idPessoa
FULL OUTER JOIN pronto_socorro c ON a.idPessoa=c.idPessoa
FULL OUTER JOIN consulta_eletiva d ON a.idPessoa=d.idPessoa
FULL OUTER JOIN flag_internacao e ON a.idPessoa= e.idPessoa
FULL OUTER JOIN cronicos f ON a.idPessoa=f.idPessoa
FULL OUTER JOIN sinistro_12_meses h ON a.idPessoa=h.idPessoa
WHERE (contumaz_eletiva=1
       OR contumaz_ps=1
       OR contumaz_exames=1)
 
 
 
 
