from google.cloud import bigquery
import pandas as pd
import datetime as dt
from dateutil.relativedelta import relativedelta


# read queries in .txt
def read_query(path_to_query):
    
    '''path_to_query: /path/to/query'''
    
    query_read = open(path_to_query, 'r', encoding='utf-8')
    sql_query = query_read.read()
    query_read.close()
    
    return sql_query


# query in bq
def bq_query_to_df(query, project, dataset, credentials):
    
    '''query: query on bq
       project: gcp project_id
       dataset: bq dataset_id
       credentials: path/to/.json'''
    
    query = query.format(project=project, dataset=dataset)
    client = bigquery.Client.from_service_account_json(json_credentials_path=credentials)
    results_df = client.query(query).to_dataframe()
    df = results_df
    print(df.head())
    return df


