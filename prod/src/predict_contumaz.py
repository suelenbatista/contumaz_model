from fastapi import HTTPException
from src.data_prep.data_prep_utils import read_query, bq_query_to_df
from src.prediction.predicting_functions import predicting_xgBoost
from src.utils.gcp_utils import upload_file, download_blob
from src.utils.api_utils import PandasToJson
import pandas as pd
import os

# run download model
def run_gcp_download_model(settings):
    
    source_file = f'{settings.CONTUMAZ_FOLDER_NAME}/ml_models/xgBoost_model.joblib'
    destination_file = f'{settings.ROOT_DIR}/src/ml_models/xgBoost_model.joblib'
    
    download_blob(bucket_name = settings.GCLOUD_BUCKET, 
                  source_blob_name = source_file, 
                  destination_file_name = destination_file, 
                  credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'file .joblib downloaded from gcp')






# rodar os processos
def run(empresa_id, filter_date, settings):
    
     # data aquisition
    query_path = f'{settings.ROOT_DIR}/src/data_prep/queries/query_contumaz_predict.txt'
    sql_query = read_query(query_path)
    sql_query = sql_query.replace('NUMBER_ID_EMPRESA', str(empresa_id))
    
    df = bq_query_to_df(query = sql_query, 
                        project = settings.GCLOUD_PROJECT, 
                        dataset = settings.BIGQUERY_DATASET_NAME, 
                        credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    df.set_index('idPessoa',inplace=True)
    
    if df.shape[0] == 0:
        raise HTTPException(status_code=400, detail="No data to predict. Please, enter a idEmpresa that exists in the database")

    
    # filtering date
    #if filter_date != 'None':
       # df = df[df['dataConsulta'] >= pd.to_datetime(filter_date)].reset_index(drop=True)
    
    
   
    
    ## predicting
    # download joblib
    run_gcp_download_model(settings)
    # prediction
    predicted_data = predicting_xgBoost(df, settings)
    
    
    # to json
    json_path = f'{settings.ROOT_DIR}/src/ml_models/final_data.json'
    data = PandasToJson(predicted_data, json_path)
    
#     # to json
#     json_path = f'{settings.ROOT_DIR}/src/ml_models/final_data.json'
#     with open(json_path, 'w', encoding='utf-8') as file:
#         final_data.to_json(file, force_ascii=False)
    
#     # read json
#     final_data_json = open(json_path)
#     data = json.load(final_data_json)
    
    # delete json
    os.remove(json_path)
    #print(data)
    return data
    
    #print('jow')
    
    #final_data_json = final_data.to_json()
    
    #return final_data_json
