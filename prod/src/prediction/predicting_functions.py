# api
from fastapi import HTTPException

# general
import numpy as np
import pandas as pd


# ml models
from sklearn.model_selection import GridSearchCV
import xgboost as xgb
from joblib import load


# predict model
def predicting_xgBoost(df, settings):
    
    if df.shape[0] == 0:
        raise HTTPException(status_code=400, detail="No data to predict. Please, enter a date at least 3 months from the current date")
    
    # carregar modelos
    model_path = f'{settings.ROOT_DIR}/src/ml_models/xgBoost_model.joblib'
    xgBoost_model = load(model_path)
    
    # predicting
    X = df.drop('alto_custo', axis=1)
    y_pred = xgBoost_model.predict(X)
    
    # add dataframe
    df['classificacao'] = y_pred
    
    df = df.reset_index()
    
    return df