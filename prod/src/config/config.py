from fastapi import HTTPException
import logging
import os
from functools import lru_cache
from pydantic import BaseSettings


log = logging.getLogger("uvicorn")

        
class Prod_Settings(BaseSettings):
    
    # gerais
    CONFIG_DIR: str = os.path.dirname(os.path.abspath(__file__))
    SRC_DIR: str = os.path.dirname(CONFIG_DIR)
    ROOT_DIR: str = os.path.dirname(SRC_DIR)
    CONTUMAZ_FOLDER_NAME = 'contumaz_models'
    BIGQUERY_DATASET_NAME = 'fenix_bi'
    #BIGQUERY_DATASET_TABLE_1 = 'fato_classificacao_vidas'
    #BIGQUERY_DATASET_TABLE_2 = 'fato_sinistro'
    
    # Credenciais de acesso ao GCP.
    GCLOUD_PROJECT: str = 'cosmic-shift-235317'
    GCLOUD_BUCKET: str = 'prd-ml-models'
    GCLOUD_CREDENTIALS_FILEPATH: str = f'{CONFIG_DIR}/gcloud_credentials_prd.json'

class Dev_Settings(BaseSettings):
    
    # gerais
    CONFIG_DIR: str = os.path.dirname(os.path.abspath(__file__))
    SRC_DIR: str = os.path.dirname(CONFIG_DIR)
    ROOT_DIR: str = os.path.dirname(SRC_DIR)
    CONTUMAZ_FOLDER_NAME = 'contumaz_models'
    BIGQUERY_DATASET_NAME = 'fenix_bi'
    #BIGQUERY_DATASET_TABLE_1 = 'fato_classificacao_vidas'###########################
    #BIGQUERY_DATASET_TABLE_2 = 'fato_sinistro'################################
    
    # Credenciais de acesso ao GCP.
    GCLOUD_PROJECT: str ='laboratorio-eng-dados'
    GCLOUD_BUCKET: str ='dev-ml-model'
    GCLOUD_CREDENTIALS_FILEPATH: str =f'{CONFIG_DIR}/gcloud_credentials_dev.json'


@lru_cache()
def get_settings(environment) -> BaseSettings:

    if environment == 'dev':
        log.info(f"Loading config settings from the environment: {environment}")
        return Dev_Settings()
    elif environment == 'prod':
        log.info(f"Loading config settings from the environment: {environment}")
        return Prod_Settings()
    else:
        raise HTTPException(status_code=400, detail="Please insert dev or prod in environment!")
