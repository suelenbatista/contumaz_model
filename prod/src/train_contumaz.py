from src.data_prep.data_prep_utils import read_query, bq_query_to_df
from src.training.training_functions import  training_process
from src.utils.gcp_utils import upload_file, download_blob
import pandas as pd



# run download dictionary
def run_gcp_download_dictionary(settings):
    
    source_file = f'{settings.PA_DIGITAL_FOLDER_NAME}/ml_models/dict_pa.csv'
    destination_file = f'{settings.ROOT_DIR}/src/ml_models/dict_pa.csv'
    
    download_blob(bucket_name = settings.GCLOUD_BUCKET, 
                  source_blob_name = source_file, 
                  destination_file_name = destination_file, 
                  credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'file dict_pa.csv downloaded from gcp')

    
    
# upload metrics to gcp
def run_gcp_upload_metrics(settings):
    
    # source files
    source_file_cross_val = f'{settings.ROOT_DIR}/src/ml_models/cv_model_results.csv'
    source_file_confusion_matrix = f'{settings.ROOT_DIR}/src/ml_models/cm_model_results.png'
    source_files = [source_file_cross_val, source_file_confusion_matrix]
    
    # destination files
    destination_file_cross_val = f'{settings.CONTUMAZ_FOLDER_NAME}/ml_results/cv_model_results.csv'
    destination_file_confusion_matrix = f'{settings.CONTUMAZ_FOLDER_NAME}/ml_results/cm_model_results.png'
    destination_files = [destination_file_cross_val, destination_file_confusion_matrix]
    
    # upload files
    for source, destination in zip(source_files, destination_files):
        
        upload_file(bucket_name = settings.GCLOUD_BUCKET, 
                    destination_blob_name = destination, 
                    source_file_path = source, 
                    credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
        
    print(f'ML metrics results uploaded to gcp')

    
        
# upload model to gcp
def run_gcp_upload_model(settings):
    
    # upload cross validation
    source_file = f'{settings.ROOT_DIR}/src/ml_models/xgBoost_model.joblib'
    destination_file = f'{settings.CONTUMAZ_FOLDER_NAME}/ml_models/xgBoost_model.joblib'
    
    upload_file(bucket_name = settings.GCLOUD_BUCKET, 
                destination_blob_name = destination_file, 
                source_file_path = source_file, 
                credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    
    print(f'ML model uploaded to gcp')
    
    
    


# processo para treinar o algoritmo do pa digital
def train_proc_contumaz(df, settings):
    

    
    # train model
    training_process(df, settings)
    
    print("Model trainning success")
    
    
    
# rodar os processos
def run(settings):
    
    # data aquisition
    query_path = f'{settings.ROOT_DIR}/src/data_prep/queries/query_contumaz_train.txt'
    sql_query = read_query(query_path)
    
    df = bq_query_to_df(query = sql_query, 
                        project = settings.GCLOUD_PROJECT, 
                        dataset = settings.BIGQUERY_DATASET_NAME, 
                        credentials = settings.GCLOUD_CREDENTIALS_FILEPATH)
    df.set_index('idPessoa',inplace=True)
    

    
    # training
    print('Start training...')
    train_proc_contumaz(df, settings)
    
    # save model joblib
    run_gcp_upload_model(settings)
    
    # save metrics
    run_gcp_upload_metrics(settings)
    
    print('Run trainning finished')