# Web servers.
fastapi==0.70.0
uvicorn==0.16.0
gunicorn==20.1.0
python-multipart==0.0.5

# Data analysis and ML.
numpy==1.19.5
pandas==1.3.0
unidecode==1.3.2
scikit-learn==0.24.2
python-dateutil==2.8.1
nltk==3.5

# Data reporting.
seaborn==0.11.1
matplotlib==3.4.2

# Storage.
tables==3.6.1
joblib==1.0.1

# GCP dependencies.
tqdm==4.61.1
pyarrow==4.0.1

# GCP.
google-cloud-storage==1.40.0
google-cloud-bigquery==2.20.0
google-cloud-bigquery-storage==2.5.0

# Documentation.
#sphinx_rtd_theme
#sphinx==4.0.3
