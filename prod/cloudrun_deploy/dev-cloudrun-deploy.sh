# Set working directory to be the parent directory of that in which the script is located.
cd "$(dirname "$(dirname "$(readlink -f "$0")")")"

# Authenticate.
gcloud auth activate-service-account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--key-file /home/jupyter/contumaz/prod/src/config/gcloud_credentials_dev.json


# Submit build.
gcloud builds submit \
--tag gcr.io/laboratorio-eng-dados/dev-contumaz-service \
--account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--project laboratorio-eng-dados \


# Deploy and run container.
gcloud run deploy \
dev-contumaz-service \
--image gcr.io/laboratorio-eng-dados/dev-contumaz-service \
--allow-unauthenticated \
--platform managed \
--region us-east1 \
--account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
--project laboratorio-eng-dados \
--memory 2Gi \
--timeout 20m \
--cpu 1