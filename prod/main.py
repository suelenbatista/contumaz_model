from fastapi import FastAPI
from routers import router

# fastapi app
app = FastAPI(
    title = "Contumaz",
    description="Um algoritmo para classificar contumazes prioritários",
    version="0.1"
)

app.include_router(router)