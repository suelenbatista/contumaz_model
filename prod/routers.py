from fastapi import APIRouter
from endpoints import home_page, contumaz_train, contumaz_predict,  preview_crossvalidation, preview_confusionmatrix

router = APIRouter()

router.include_router(home_page.router, tags=["Home"])
router.include_router(preview_crossvalidation.router, tags=["CONTUMAZ_CrossValidation_Results"])
router.include_router(preview_confusionmatrix.router, tags=["CONTUMAZ_ConfusionMatrix_Results"])
router.include_router(contumaz_train.router, tags=["CONTUMAZ_Train"])
router.include_router(contumaz_predict.router, tags=["CONTUMAZ_Predict"])

